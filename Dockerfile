FROM debian:jessie

RUN apt-get -y update
RUN apt-get -y dist-upgrade
RUN apt-get -y install python-dev python-pip
ADD . /opt/irklab/
WORKDIR /opt/irklab/
RUN pip install -r requirements.txt
ENV SERVER=irc.freenode.net
ENV CHANNEL=#irklabtest
ENV NICK=irklab

CMD python irklab -v -c $CHANNEL -s $SERVER -n $NICK
