Simple Gitlab to IRC gateway
============================

This webserver will listen on a specified port (default to 8080) for
Gitlab [web hooks][] and will post messages to a pre-determined IRC
channel.

 [web hooks]: https://gitlab.com/help/web_hooks/web_hooks.md

There should be little configuration required but the target channel,
as an argument on the commandline.

Requirements
------------

This also depends on the Twisted framework:

    apt-get install python-twisted-web python-twisted-words

See also [the Twisted project][]for more information. Some features
depend on upstream Twisted patches, see "Reconnection support" below
for more information.

 [the Twisted project]: https://twistedmatrix.com

Installation
------------

This should be ran directly from the source directory right now:

    ./irklab -s irc.freenode.net/ -c nothingbutuschickens

Otherwise `irklab` can also be installed in your path. One way to
accomplish this would be to install `irklab` directly in
`/usr/local/bin`, for example:

    cp -p irklab /usr/local/bin

The `setup.py` command will install all those for you automatically in
`/usr/local/bin` (by default):

    ./setup.py install --record=install.log

The daemon is designed to run in the foreground, use Debian's
start-stop-daemon(8) or systemd(1) to run it as a daemon. A
`irklab.service` file is provided for the latter.

Then configure a web hook to point to the irklab service, for example:

    http://example.com:8080/irc.example.com/channel

...will make the bot connect to irc.example.com/#channel for this
specific hook. Just specifying:

    http://example.com:8080/

...will connect to the server and channel configured on the commandline
(which will of course fail if none are configured).

Reconnection support
--------------------

There is currently no official way for Twisted endpoints to
automatically reconnect on failure or on connection loss. This is a
[known problem (#7840)][] in Twisted code that affects irklab.

To workaround this, I have been experimenting with the
[ReconnectingClientService patch from #4735][]. Using [virtualenv][],
for example, this will look something like this:

    apt-get install virtualenv
    virtualenv irklab
    . ./irklab/bin/activate
    git clone -b persistent-client-service-4735-4 https://github.com/twisted/twisted.git
    cd twisted && python ./setup.py
    python ./setup.py
    ./irklab

Now of course you will need to run this always within the virtualenv,
but the new Twisted reconnexion code will be automatically detected
and used.

 [known problem (#7840)]: https://twistedmatrix.com/trac/ticket/7840
 [ReconnectingClientService patch from #4735]: https://twistedmatrix.com/trac/ticket/4735

Ideas
-----

The "IRC url" (irc.example.com/channel) should be taken from the hook
URL, with defaults set on the commandline. Right now, trailing
characters after / in the hook URL are parsed as a server/channel,
where the server part is optional. It uses the configured port, so may
not work on all IRC servers.

Some improvements on how this is managed could be taken from the
Resource handling code in twisted, for example, something like this
was suggested:

    def getChild(self, path, request):
        if not path in KNOWN_SERVERS: # we're serving a /channel request
            conn = bots['server']
            return ChannelResource(conn)
    
        if path in bots:
            server_addr = SERVERS[path]
            ep = TCP4ClientEndpoint(reactor, server_addr, port)
            d = ep.connect(IRCFactory(path)).addCallback(ChannelResource)
            return DeferredResource(d)
        else:
            return ChannelResource(bots[path])

Also, the callback system may be clarified with a DeferredQueue():

    class Proto:
        def __init__(self):
            self.tojoin = defer.DeferredQueue()
    
        @defer.inlineCallbacks
        def signedOn(self):
            while True:
                channelToJoin = yield self.tojoin.get()
                self.join(channelToJoin)
    
    proto.tojoin.put('#channel')

But this will imply tighter coupling in the code, which i don't like.

Credits
-------

Many thanks to `asdf` on IRC for the help with the `defer` subsystem.
